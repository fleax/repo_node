var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/clientes', function(req, res) {
  clientes = [
    {
      id: 1,
      nombre: "Cliente 1",
      apellidos: "Apellidos 1"
    },
    {
      id: 2,
      nombre: "Cliente 2",
      apellidos: "Apellidos 2"
    },
    {
      id: 3,
      nombre: "Cliente 3",
      apellidos: "Apellidos 3"
    }
  ];
  res.send(clientes);
});

app.get('/clientes/:id', function(req, res) {
  res.send("Cliente " + req.params.id);
});

app.post('/clientes', function(req, res) {
  res.send("Hemos dado de alta el cliente");
});

app.put('/clientes/:id', function(req, res) {
  res.send("Hemos modificado el cliente  " + req.params.id);
});

app.delete('/clientes/:id', function(req, res) {
  res.send("Hemos dado de baja el cliente " + req.params.id);
});
