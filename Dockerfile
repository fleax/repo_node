# Imagen base
FROM node:latest

# Directorio de la app
WORKDIR /app

# Copiar archivos
ADD . /app

# Dependencias
RUN apt-get update && apt-get install -y vim
RUN npm install

# Puerto
EXPOSE 3000

# Comando
CMD ["npm", "start"]
